FROM node

WORKDIR /var/app/
COPY .babelrc .
COPY package.json .

RUN npm install -g babel-cli
RUN npm install

CMD ["npm", "start"]