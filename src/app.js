import express     from 'express';
import debugModule from 'debug';

const EXPRESS_PORT = 80;
const debug        = debugModule('server:app');
const app          = express();

app.listen(EXPRESS_PORT, () => debug(`App qui roule n'amasse pas mousse sur le port ${EXPRESS_PORT}`));
